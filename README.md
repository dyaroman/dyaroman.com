# [dyaroman.com](https://dyaroman.com)

[![Build Status](https://travis-ci.org/dyaroman/dyaroman.com.svg?branch=master)](https://travis-ci.org/dyaroman/dyaroman.com)

### Get started

run `npm ci` in root directory  

### How to use

`npm run build` - build all templates from `./src/` and put them to `./dest/`  
`npm run build:prod` - build all templates, optimize `css` and `js` for production  
`npm run dev` - build template and watch changes, also run dev server on `localhost:8080`  
